import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import axios from './axios-api';
import {connectRouter, routerMiddleware, ConnectedRouter} from 'connected-react-router';
import {createStore,applyMiddleware, compose, combineReducers} from 'redux';
import {createBrowserHistory} from 'history';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {saveToLocalStorage,loadFromLocalStorage} from "./store/localStorage";

import albumReducer from './store/reducers/albumReducer';
import artistReducer from './store/reducers/artistReducer';
import trackReducer from './store/reducers/trackReducer';
import track_historyReducer from './store/reducers/track_historyReducer';
import usersReducer from './store/reducers/usersReducer';

const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    artists: artistReducer,
    albums: albumReducer,
    tracks: trackReducer,
    track_history: track_historyReducer,
    users: usersReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    });
});

axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token;
    } catch (e) {
        // do nothing, user is not logged in
    }

    return config;
});

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();