import {FETCH_ALBUM_SUCCESS, PUBLISH_ALBUM_SUCCESS} from "../actions/albumActions";

const initialState ={
    albums: []
};

const albumReducer = (state = initialState,action)=>{
    switch (action.type) {
        case FETCH_ALBUM_SUCCESS:
            return{...state,albums: action.albums};
        case PUBLISH_ALBUM_SUCCESS:
            return{...state,albums:action.albums};
        default: return state;
    }
};

export default albumReducer;