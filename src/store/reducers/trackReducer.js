import {
    CREATE_TRACK_SUCCESS,
    FETCH_TRACKS_SUCCESS,
    PUBLISH_TRACK_SUCCESS,
}
    from "../actions/trackActions";

const initialState ={
    tracks: null,
};

const trackReducer = (state = initialState,action)=>{
    switch (action.type) {
        case FETCH_TRACKS_SUCCESS:
            return{...state,tracks: action.tracks};
        case CREATE_TRACK_SUCCESS:
            return{...state,tracks:action.tracks};
        case PUBLISH_TRACK_SUCCESS:
            return{...state,tracks:action.tracks};
        default: return state;
    }
};

export default trackReducer;