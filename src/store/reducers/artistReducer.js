import {FETCH_ARTIST_SUCCESS, PUBLISH_ARTIST_SUCCESS} from "../actions/artistActions";

const initialState ={
  artists: []
};

const artistsReducer = (state = initialState,action)=>{
  switch (action.type) {
      case FETCH_ARTIST_SUCCESS:
          return{...state,artists: action.artists};
      case PUBLISH_ARTIST_SUCCESS:
          return{...state,tracks:action.artists};
      default: return state;
  }
};

export default artistsReducer;