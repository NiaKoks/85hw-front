import {FETCH_HISTORY_SUCCESS,ADD_TO_PLAYLIST} from "../actions/track_historyActions"

const initialState ={
    history: null,
    heard: null
};

const track_historyReducer = (state = initialState,action)=>{
    switch (action.type) {
        case FETCH_HISTORY_SUCCESS:
            return{...state,history: action.tracks};
        case ADD_TO_PLAYLIST:
            return{...state,heard: action.heard};
        default: return state;
    }
};

export default track_historyReducer;