import axios from '../../axios-api';
import {fetchTracks} from "./trackActions";

export const FETCH_ALBUM_SUCCESS = "FETCH_ALBUM_SUCCESS";
export const CREATE_ALBUM_SUCCESS = "CREATE_ALBUM_SUCCESS";
export const PUBLISH_ALBUM_SUCCESS = "PUBLISH_ALBUM_SUCCESS";

export const fetchAlbumSuccess = albums =>({type:FETCH_ALBUM_SUCCESS,albums});
export const createAlbumSuccess = albums =>({type:CREATE_ALBUM_SUCCESS,albums});
export const publishAlbumSuccess = albums =>({type:PUBLISH_ALBUM_SUCCESS,albums});

export const fetchAlbum = (id) =>{
    return dispatch =>{
        return axios.get(`/albums?artist=${id}`).then(response => dispatch(fetchAlbumSuccess(response.data)))
    };
};

export const createAlbum = albumsData =>{
    return dispatch =>{
        return axios.post('/albums',albumsData).then(
            () => dispatch(createAlbumSuccess())
        )
    }
};

export const deleteAlbum = (id,artistID) =>{
    return dispatch =>{
        return axios.delete(`/albums/${id}`)
            .then(()=>dispatch(fetchAlbum(artistID)))
    };
};
export const publishAlbum = (id,albumID) =>{
    return dispatch =>{
        return axios.post(`/tracks/${id}`).then(()=>dispatch(fetchTracks(albumID)))
    }
};