import axios from '../../axios-api';
import {fetchTracks} from "./trackActions";

export const FETCH_ARTIST_SUCCESS = "FETCH_ARTIST_SUCCESS";
export const CREATE_ARTIST_SUCCESS = "CREATE_ARTIST_SUCCESS";
export const PUBLISH_ARTIST_SUCCESS = "PUBLISH_ARTIST_SUCCESS";

export const fetchArtistSuccess = artists =>({type:FETCH_ARTIST_SUCCESS,artists});
export const createArtistSuccess = artists =>({type:CREATE_ARTIST_SUCCESS,artists});
export const publishArtistSuccess = artists =>({type:PUBLISH_ARTIST_SUCCESS,artists});

export const fetchArtist = () =>{
  return dispatch =>{
      return axios.get('/artists').then(response => dispatch(fetchArtistSuccess(response.data)))
  };
};

export const createArtist = artistsData =>{
    return dispatch =>{
        return axios.post('/artists',artistsData).then(
            () => dispatch(createArtistSuccess())
        )
    }
};

export const deleteArtist = (id,artistID) =>{
    return dispatch =>{
        return axios.delete(`/artists/${id}`)
            .then(()=>dispatch(fetchArtist(artistID)))
    };
};

export const publishArtist = (id,artistID) =>{
    return dispatch =>{
        return axios.post(`/artists/${id}`).then(()=>dispatch(fetchTracks(artistID)))
    }
};