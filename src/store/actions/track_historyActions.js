import {push} from 'connected-react-router';
import axios from '../../axios-api';

export const FETCH_HISTORY_SUCCESS = "FETCH_HISTORY_SUCCESS";
export const ADD_TO_PLAYLIST = "ADD_TO_PLAYLIST";

export const fetchHistorySuccess = tracks =>({type:FETCH_HISTORY_SUCCESS,tracks});
export const addToPlayList = tracks =>({type:ADD_TO_PLAYLIST,tracks});

export const fetchHistory = () =>{
    return (dispatch, getState) =>{
        const user = getState().users.user;
        if (user === null){
            dispatch(push('/login'))
        } else {
        return axios.get(`/tracks_history`, {headers:{'Authorization': user.token}}).then(response => dispatch(fetchHistorySuccess(response.data)))
        }
    };
};

export const addToPlaylist = id =>{
    return (dispatch,getState) =>{
        const user = getState().users.user;
        if (user === null){
            dispatch(push('/login'))
        } else {
            return axios.post('/tracks_history',{trackID: id},{headers:{'Authorization':user.token}}).then(
                ()=>{
                    dispatch(push('/'))
                }
            )
        }
    }
};