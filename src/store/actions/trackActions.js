import axios from '../../axios-api';

export const FETCH_TRACKS_SUCCESS = "FETCH_TRACKS_SUCCESS";
export const CREATE_TRACK_SUCCESS = "CREATE_TRACK_SUCCESS";
export const PUBLISH_TRACK_SUCCESS = "PUBLISH_TRACK_SUCCESS";


export const fetchTrackSuccess = tracks =>({type:FETCH_TRACKS_SUCCESS,tracks});
export const createTrackSuccess = tracks =>({type:CREATE_TRACK_SUCCESS,tracks});
export const publishTrackSuccess = tracks =>({type:PUBLISH_TRACK_SUCCESS,tracks});


export const fetchTracks = (id) =>{
    return dispatch =>{
        return axios.get(`/tracks?album=${id}`).then(response => dispatch(fetchTrackSuccess(response.data)))
    };
};

export const createTrack = tracksData =>{
    return dispatch =>{
        return axios.post('/tracks',tracksData).then(
            () => dispatch(createTrackSuccess())
        )
    }
};
export const deleteTrack = (id,albumID) =>{
  return dispatch =>{
      return axios.delete(`/tracks/${id}`)
                        .then(()=>dispatch(fetchTracks(albumID)))
  };
};

export const publishTrack = (id,trackID) =>{
    return dispatch =>{
        return axios.post(`/tracks`).then(()=>dispatch(fetchTracks(trackID)))
    }
};