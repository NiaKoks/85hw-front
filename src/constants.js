export const apiURL = 'http://localhost:8000';

export const style ={
    button: {
        marginTop: '100px',
        marginLeft: '5px'
    },
    trackButton: {
        marginTop: '10px',
        marginLeft: '5px'
    },
    albumButton: {
       float: 'right',
       marginRight:'10px'
    },
    card:{
        padding:'15px',
        marginLeft: '10px',
        marginTop: '10px',
        display:'inline-block'
    },
    album:{
        paddingLeft:'10px',
        marginTop: '10px',
        marginLeft:'5%',
        marginRight:'5%'
    },
    adminText:{
        fontWeight: '700',
        textAlign: 'center',
        textTransform: 'uppercase',
        color: 'crimson',
        marginBottom:'-18px',
        paddingBottom: '10px'
    }
};
