import React, { Component,Fragment} from 'react';
import {Route,Switch,withRouter} from "react-router-dom";
import {NotificationContainer} from 'react-notifications';
import Toolbar from "../src/components/UI/Toolbar/Toolbar";
import AddNewTrack from "./containers/AddOptions/NewTrack/AddNewTrack";
import AddNewAlbum from "./containers/AddOptions/NewAlbum/AddNewAlbum";
import AddNewArtist from "./containers/AddOptions/NewArtist/AddNewArtist";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register"
import AllArtists from "./containers/AllArtits/AllArtists";
import AllAlbums from "./containers/AllAlbums/AllAlbums";
import AllTracks from "./containers/AllTracks/AllTracks";
import TrackHistory from "./containers/UserTrackHistory/TrackHistory";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/userActions";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar user={this.props.user} logout={this.props.logout}/></header>
          <NotificationContainer/>
          <Switch>
            <Route path="/" exact component={AllArtists}/>

            <Route path="/tracks/new" exact component={AddNewTrack}/>
            <Route path="/albums/new" exact component={AddNewAlbum}/>
            <Route path="/artists/new" exact component={AddNewArtist}/>

            <Route path="/artists/:id" exact component={AllAlbums}/>
            <Route path="/tracks/:id" exact component={AllTracks}/>

            <Route path="/tracks_history" exact component={TrackHistory}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
          </Switch>
      </Fragment>
    );
  }
}

const mapStateToProps = state =>({
  user: state.users.user
});
const mapDispatchToProps = dispatch =>({
  logout: () => dispatch(logoutUser())
});
export default  withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
