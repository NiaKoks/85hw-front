import React, {Component,Fragment} from 'react';

import {createTrack} from "../../../store/actions/trackActions";
import {fetchAlbum} from "../../../store/actions/albumActions";

import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";



class AddNewTrack extends Component {
    state={
        name:'',
        album: '',
        long: '',
        trackNo:'',
        yt_link: '',
    };

    componentDidMount() {
        this.props.fetchAlbum();
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.createTrack({...this.state});
    };

    createTrack = tracksData => {
        this.props.onTrackCreated(tracksData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h3>Add New Track</h3>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="name">Name:</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="name" id="name"
                                placeholder="Input name of the artist or group"
                                value={this.state.name}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label sm={2} for='album'>Album:</Label>
                        <Col sm={10}>
                            <Input
                                type="select" required
                                name="album" id="album"
                                value={this.state.album} onChange={this.inputChangeHandler}
                            >
                                <option value="">Please select album...</option>
                                    {this.props.albums.map(album =>(
                                <option key={album._id} value={album._id}>{album.name}</option>
                                    ))}
                            </Input>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label sm={2} for="trackNo">track No:</Label>
                        <Col sm={10}>
                            <Input
                                type="number" required min="0"
                                name="trackNo" id="trackNo"
                                placeholder="Enter  track No"
                                value={this.state.trackNo} onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="long">Lenght of composition:</Label>
                        <Col sm={10}>
                            <Input
                                type="string"
                                name="long" id="long"
                                placeholder="Enter lenght of composition"
                                value={this.state.long}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="yt_link">YouTube Link:</Label>
                        <Col sm={10}>
                            <Input
                                type="string"
                                name="yt_link" id="yt_link"
                                placeholder="Enter YouTube link for composition"
                                value={this.state.yt_link}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col>
                            <Button type="submit" color="primary">Upload</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state =>({
    albums:state.albums.albums,
});


const mapDispatchToProps = dispatch =>({
    onTrackCreated: tracksData =>dispatch(createTrack(tracksData)),
    fetchAlbum: () =>dispatch(fetchAlbum())
});

export default connect(mapStateToProps,mapDispatchToProps)(AddNewTrack);