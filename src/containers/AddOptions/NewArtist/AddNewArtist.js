import React, {Component,Fragment} from 'react';

import {connect} from "react-redux";
import {createArtist} from "../../../store/actions/artistActions";

import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class AddNewArtist extends Component {

    state={
        name: '',
        info: '',
        photo: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.createArtist(formData);
    };

    createArtist = artistsData => {
        this.props.onArtistCreated(artistsData).then(() => {
            this.props.history.push('/');
        });
    };


    render() {
        return (
            <Fragment>
                <h3>Add New Artist</h3>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label sm={2} for="name">Name:</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="name" id="name"
                                placeholder="Input name of the artist"
                                value={this.state.name}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="info">Information:</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="info" id="info"
                                placeholder="Input some information about artist"
                                value={this.state.info}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="photo">Artist Photo:</Label>
                        <Col sm={10}>
                            <Input
                                type="file"
                                name="photo" id="photo"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col>
                            <Button type="submit" color="primary">Upload</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch =>({
    onArtistCreated: artistsData =>dispatch(createArtist(artistsData)),
});

export default connect(null,mapDispatchToProps)(AddNewArtist);