import React, {Component,Fragment} from 'react';

import {createAlbum} from "../../../store/actions/albumActions";
import {fetchArtist} from "../../../store/actions/artistActions";

import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class AddNewAlbum extends Component {
    state={
      name:'',
      artist: '',
      year:'',
      cover_img:'',
    };

    componentDidMount() {
        this.props.fetchArtist();
    }
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.createAlbum(formData);
    };

    createAlbum = albumData => {
        this.props.onAlbumCreated(albumData).then(() => {
            this.props.history.push('/');
        });
    };


    render() {
        return (
                <Fragment>
                    <h3>Add New Album</h3>
                    <Form onSubmit={this.submitFormHandler}>
                        <FormGroup row>
                            <Label sm={2} for="name">Name:</Label>
                            <Col sm={10}>
                                <Input
                                    type="text"
                                    name="name" id="name"
                                    placeholder="Input name of the artist or group"
                                    value={this.state.name}
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Label sm={2} for='artist'>Artist/Group:</Label>
                        <Col sm={10}>
                         <Input
                            type="select" required
                            name="artist" id="artist"
                            value={this.state.artist} onChange={this.inputChangeHandler}
                         >
                            <option value="">Please select artist...</option>
                                {this.props.artists.map(artist =>(
                            <option key={artist._id} value={artist._id}>{artist.name}</option>
                                ))}
                         </Input>
                        </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} for="year">Year:</Label>
                            <Col sm={10}>
                                <Input
                                    type="number" required min="0"
                                    name="year" id="year"
                                    placeholder="Enter  year"
                                    value={this.state.year} onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} for="cover_img">Album cover:</Label>
                            <Col sm={10}>
                                <Input
                                    type="file"
                                    name="cover_img" id="cover_img"
                                    onChange={this.fileChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col>
                                <Button type="submit" color="primary">Upload</Button>
                            </Col>
                        </FormGroup>
                    </Form>
            </Fragment>
        );
    }
}
const mapStateToProps = state =>({
    artists:state.artists.artists
});

const mapDispatchToProps = dispatch =>({
    onAlbumCreated: albumsData =>dispatch(createAlbum(albumsData)),
    fetchArtist: () =>dispatch(fetchArtist())
});

export default connect(mapStateToProps,mapDispatchToProps)(AddNewAlbum);