import React, {Component, Fragment} from 'react';
import ArtistCard from "../../components/Cards/ArtistCard/ArtistCard";
import {connect} from 'react-redux';
import {deleteArtist, fetchArtist, publishArtist} from "../../store/actions/artistActions";


class AllArtists extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchArtists(id);
    }

    render  () {
        return (
            <Fragment>
                <ArtistCard artists={this.props.artists}
                            user={this.props.user}
                            delete={this.props.deleteArtist}
                            publish={this.props.publishArtist}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        artists: state.artists.artists,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchArtists: () => dispatch(fetchArtist()),
        deleteArtist: (id,artistID) => dispatch(deleteArtist(id,artistID)),
        publishArtist: (id,artistID) => dispatch(publishArtist(id,artistID)),
    }
};

export default  connect(mapStateToProps, mapDispatchToProps)(AllArtists);