import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {deleteTrack, fetchTracks, publishTrack} from "../../store/actions/trackActions";
import TracksCard from "../../components/Cards/TracksCard/TracksCard";
import {addToPlaylist} from "../../store/actions/track_historyActions";


class AllTracks extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchTracks(id);
    }
    render  () {
        return (
            <Fragment>
                <TracksCard
                    tracks={this.props.tracks}
                    addToPlaylist={this.props.addToPlaylist}
                    delete={this.props.deleteTrack}
                    publish={this.props.publishTrack}
                    user={this.props.user}
                />
            </Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks,
        user: state.users.user
    }
};
const mapDispatchToProps = dispatch => {
    return {
        fetchTracks: (id) => dispatch(fetchTracks(id)),
        addToPlaylist: (id) => dispatch(addToPlaylist(id)),
        deleteTrack: (id,albumID) => dispatch(deleteTrack(id,albumID)),
        publishTrack:(id,albumID) => dispatch(publishTrack(id,albumID))
    }
};


export default  connect(mapStateToProps, mapDispatchToProps)(AllTracks);