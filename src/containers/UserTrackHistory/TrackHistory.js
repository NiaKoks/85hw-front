import React, {Component,Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchHistory} from "../../store/actions/track_historyActions";

class TrackHistory extends Component {

    componentDidMount() {
        this.props.fetchHistory();
    }
    render  () {
        return (
            <Fragment>
                <h2>your playlist</h2>
                {this.props.tracks && this.props.tracks.map(
                    (track,index) =>{
                        return(
                            <div>{track.track.album.artist.name} - {track.track.name}</div>
                        )
                    })
                }

            </Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        tracks: state.track_history.history,
        heard: state.tracks.heard
    }
};
const mapDispatchToProps = dispatch => {
    return {
        fetchHistory: () => dispatch(fetchHistory()),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);