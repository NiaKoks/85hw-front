import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import AlbumCard from "../../components/Cards/AlbumCard/AlbumCard";
import {deleteAlbum, fetchAlbum, publishAlbum} from "../../store/actions/albumActions";


class AllAlbums extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchAlbums(id);
    }

    render  () {
        return (
            <Fragment>
                <AlbumCard
                    albums={this.props.albums}
                    user={this.props.user}
                    delete={this.props.deleteAlbum}
                    publish={this.props.publishAlbum}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAlbums: (id) => dispatch(fetchAlbum(id)),
        deleteAlbum: (id,artistID) => dispatch(deleteAlbum(id,artistID)),
        publishAlbum: (id,artistID) => dispatch(publishAlbum(id,artistID))
    }
};

export default  connect(mapStateToProps, mapDispatchToProps)(AllAlbums);