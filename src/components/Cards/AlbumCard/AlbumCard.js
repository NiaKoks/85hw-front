import React,{Fragment} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, CardBody, CardSubtitle} from "reactstrap";
import {Link} from "react-router-dom";
import Thumbnail from "../../Thumbnail/Thumbnail";
import {style} from "../../../constants";


const AlbumCard = (props) => {
    return (
        props.albums ? props.albums.map((album, index) => {
            return (
                <Fragment key={index}>

                    <Card style={style.album}>
                        <h2>{album.artist.name}</h2>
                        <CardBody>
                            {!album.published && <CardSubtitle style={style.adminText}>waiting approval</CardSubtitle>}
                            <Thumbnail image={album.cover_img}/>
                            <p>{album.year}</p>
                            <Link to={'/tracks/' + album._id}>{album.name}</Link>
                            {props.user && props.user.role ==='admin' &&
                            <Fragment>
                                <Button type="submit" color="success" style={style.albumButton}
                                        onClick={event=>{event.stopPropagation();
                                            // album.published =  this.setState({published:true})
                                            props.publish(album._id,album.artist._id)
                                        }}
                                >
                                    Post
                                </Button>
                                <Button color="danger" style={style.albumButton}
                                        onClick={event=> { event.stopPropagation();
                                            props.delete(album._id,album.artist._id)}}
                                >
                                    Delete
                                </Button>
                            </Fragment>
                            }
                        </CardBody>
                    </Card>
                }

                </Fragment>
            )
        }) : null
    );
};
AlbumCard.propTypes ={
    cover_img: PropTypes.string
};
export default AlbumCard;

