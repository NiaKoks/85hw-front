import React,{Fragment} from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody, CardTitle, CardText, Modal, Button, CardSubtitle} from "reactstrap";
import {style} from "../../../constants";

const TracksCard = (props) => {
    return (
        props.tracks ? props.tracks.map((track, index) => {
            return (
                <Fragment key={index}>
                    {track.published === true &&
                    <Card style={style.card}>
                        <h3>{track.album.name}</h3>
                        <CardBody onClick={()=>props.addToPlaylist(track._id)}>
                        <CardText>{track.trackNo}</CardText>
                        <hr/>
                        <CardTitle>{track.name}</CardTitle>
                        <hr/>
                        <CardText>{track.long} min</CardText>
                        <Modal>{track.yt_link}</Modal>
                        </CardBody>
                        </Card>
                    } : { props.user && props.user.role ==='admin' && track.published === false &&

                    <Card style={style.card}>
                        <h3>{track.album.name}</h3>
                        <CardBody onClick={()=>props.addToPlaylist(track._id)}>

                            <CardSubtitle style={style.adminText}>waiting approval</CardSubtitle>

                            <CardText>{track.trackNo}</CardText>
                            <hr/>
                            <CardTitle>{track.name}</CardTitle>
                            <hr/>
                            <CardText>{track.long} min</CardText>
                            <Modal>{track.yt_link}</Modal>
                            {props.user && props.user.role ==='admin' &&
                            <Fragment>
                                <Button type="submit" color="success" style={style.trackButton}
                                        onClick={event=>{event.stopPropagation();
                                            this.setState({published:true});
                                            props.publish(track._id,track.album._id)
                                        }}
                                >
                                    Post
                                </Button>
                                <Button color="danger" style={style.trackButton}
                                        onClick={event => { event.stopPropagation();
                                            props.delete(track._id,track.album._id) }
                                        }>
                                    Delete
                                </Button>
                            </Fragment>
                            }
                        </CardBody>
                    </Card>
                }

                </Fragment>

            )
        }): null
    );
};
TracksCard.propTypes ={
    photo: PropTypes.string,
    trackNo: PropTypes.number,
    long: PropTypes.number,
    yt_link: PropTypes.string,
    addToPlaylist: PropTypes.func
};
export default TracksCard;

