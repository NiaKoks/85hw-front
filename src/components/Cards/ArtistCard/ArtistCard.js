import React, {Fragment} from 'react';
import {Button, Card, CardBody, CardSubtitle} from "reactstrap";
import {Link} from "react-router-dom";
import Thumbnail from "../../Thumbnail/Thumbnail";
import {style} from "../../../constants";

const ArtistCard = (props) => {
    return (
        props.artists ? props.artists.map((artist, index) => {
            return (
                <Fragment key={index}>
                    <Card style={style.card}>
                        <CardBody>
                            {artist.published === false &&
                            <CardSubtitle style={style.adminText}>waiting approval</CardSubtitle>
                            }
                            <Thumbnail image={artist.photo}/>
                            <Link to={'/artists/' + artist._id}>
                                {artist.name}
                            </Link>
                            {props.user && props.user.role ==='admin' &&
                            <Fragment>
                                <Button type="submit" color="success" style={style.button}
                                    // onClick={ artist.published = this.setState({published:true})}
                                >
                                    Post
                                </Button>
                                <Button
                                    color="danger" style={style.button}
                                    onClick={event=> { event.stopPropagation();
                                        props.delete(artist._id)
                                    }}
                                >
                                    Delete
                                </Button>
                            </Fragment>
                            }
                        </CardBody>
                    </Card>
                </Fragment>
            )
        }) : null
    );
};
export default ArtistCard;

