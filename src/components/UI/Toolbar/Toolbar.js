import React ,{Fragment} from 'react';
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink,
    UncontrolledDropdown
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

import './Toolbar.css';

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Music Box</NavbarBrand>

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Artists</NavLink>
                </NavItem>

                {user ? (
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            <div className="avatar">
                                <img src={user.avatar} alt=""/>
                            </div>
                            Hello, {user.username}!
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/artists/new">
                                    Add New Artist
                                </NavLink>
                            </DropdownItem>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/albums/new">
                                    Add New Album
                                </NavLink>
                            </DropdownItem>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/tracks/new">
                                    Add New Track
                                </NavLink>
                            </DropdownItem>

                            <DropdownItem>
                               <NavLink tag={RouterNavLink} to="/tracks_history">
                                   My Track History
                               </NavLink>
                            </DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem onClick={logout}>
                                Logout
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                ) :(
                    <Fragment>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/register" exact>Sign Up</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/login" exact>Login</NavLink>
                        </NavItem>
                    </Fragment>
                )}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;